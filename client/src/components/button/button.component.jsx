import React from 'react';
import {ButtonContainer} from "./button.styles";
import './button.styles.scss';

const Button = ({ children, ...props }) => (
    <ButtonContainer {...props}>{children}</ButtonContainer>
);

export default Button;