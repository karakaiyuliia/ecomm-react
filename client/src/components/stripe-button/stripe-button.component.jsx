import React from "react";
import StripeCheckout from "react-stripe-checkout";
import axios from "axios";

const StripeCheckoutButtonComponent = ({price}) => {
    const priceForStripe = price * 100;
    const key = 'pk_test_51K1hJNH8aF5wCfyeJ48SWqHCuDa49rvsBwsZgeFlgwu4qmfp3yn3Vm5OhmhEo87666lT49AztxNI4nfcY8f1J3XN00iJDtCQd6';

    const onToken = token => {
        axios({
            url: 'payment',
            method: 'post',
            data: {
                amount: priceForStripe,
                token: token
            }
        }).then(response => {
            alert("Payment successfull")
        }).catch(error => {
            console.log(error)
            alert("There has been an issue with your payment")
        })
    }

    return (
        <StripeCheckout label='Pay Now' name='CRWN Clothing Ltd.'
                        billingAddress shippingAddress
                        image='https://iconscout.com/icon/pay-6'
                        description={`Your total is ${price}`}
                        amount={priceForStripe}
                        panelLabel='Pay Now'
                        token={onToken} stripeKey={key}
        />
    )
}

export default StripeCheckoutButtonComponent;