import React, {  useState } from 'react';
import {connect} from "react-redux";
import FormInput from "../form-input/form-input.component";
import Button from "../button/button.component";
import { signUpStart } from "../../redux/user/user.actions";

import './sign-up.styles.scss';

const SignUp = ({ signUpStart }) => {
    const [userCredentials, setUserCredentials] = useState({
        displayName: '',
        email: '',
        password: '',
        confirmPassword: ''
    });
    const { displayName, email, password, confirmPassword } = userCredentials;

    const handleSubmit = async event => {
        event.preventDefault();

        if(password !== confirmPassword) {
            alert("Passwords don't match!");
            return;
        }

        signUpStart({ displayName, email, password });
    }

    const handleChange = (event) => {
        const { name, value } = event.target;

        setUserCredentials({...userCredentials, [name]: value});
    }

    return (
        <div className='sign-up'>
            <h2 className='title'>I do not have an account</h2>
            <span>Sign up with your email and password</span>

            <form className='sign-up-form' onSubmit={handleSubmit}>
                <FormInput onChange={handleChange} type='text' name='displayName' value={displayName} label='Display Name' required/>
                <FormInput onChange={handleChange} type='email' name='email' value={email} label='Email' required/>
                <FormInput onChange={handleChange} type='password' name='password' value={password} label='Password' required/>
                <FormInput onChange={handleChange} type='password' name='confirmPassword' value={confirmPassword} label='Confirm password' required/>
                <Button type='submit'>SIGN UP</Button>
            </form>
        </div>
    )
}

const mapDispatchToProps = dispatch => ({
    signUpStart: userCredentials => dispatch(signUpStart(userCredentials))
});

export default connect(null, mapDispatchToProps)(SignUp);