import { createStore, applyMiddleware } from "redux";
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { fetchCollectionsStart } from "../pages/shop/shop.sagas";
import rootSaga from "./root-saga";
import { persistStore } from "redux-persist";
import rootReducer from './root-reducer';

const sagaMiddleware = createSagaMiddleware();

const middleWares = [logger, sagaMiddleware];
export const store = createStore(rootReducer, applyMiddleware(...middleWares));

sagaMiddleware.run(rootSaga);

export const persistor = persistStore(store);

export default { store, persistor };