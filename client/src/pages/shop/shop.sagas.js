import { takeLatest, call, put } from 'redux-saga/effects';
import ShopActionTypes from "../../redux/shop/shop.types";
import {convertCollectionsSnapshotToMap, firestore} from "../../firebase/firebase.util";
import {fetchCollectionsFailure, fetchCollectionsSuccess} from "../../redux/shop/shop.actions";

export function* fetchCollectionsAsync() {
    try {
        const collectionRef = firestore.collection('collections');
        const snapshot = yield collectionRef.get();
        const collectionsMap = yield call(convertCollectionsSnapshotToMap, snapshot);
        yield put(fetchCollectionsSuccess(collectionsMap));
    } catch(error) {
        yield put(fetchCollectionsFailure(error.message))
    }
}

export function* fetchCollectionsStart() {
    yield takeLatest(ShopActionTypes.FETCH_COLLECTIONS_START, fetchCollectionsAsync);
}